var express = require('express'),
  mongoose = require('mongoose'),
  Products = mongoose.model('Products'),
  ObjectId = require('mongodb').ObjectId;


exports.getRecords = function(req,res){
    Products.getRecords(req, function(err,result){
        result = Products.replaceId(result);
        res.send(result);
    });
}

exports.getCategories = function(req,res){
    Products.getCategories(req, function(err,result){
        res.send(result);
    });
}

exports.upload = function(req,res){
    var id = ObjectId(req.body.id_prod);
    Products.update({"_id":id}, {"image_src":req.file.filename}, function(err,result){
        res.send({filename: req.file.filename});
    });
}

exports.addRecord = function(req,res){
    Products.addRecord(req, function(err, result){
        res.send({newid:result._id});
    });
}

exports.updateRecord = function(req,res){
    var id = ObjectId(req.body.id);
    Products.update({"_id":id}, req.body, function(err, result){
        res.send({});
    });
}

exports.deleteRecord = function(req, res){
    var id = ObjectId(req.body.id);
    Products.remove({"_id":id}, function(err, result){
        res.send({});
    });
}

exports.getRecordsByCategory = function(req,res){
    Products.getOrdersByCategory(req, function(err, result){
        result = Products.helperGetOrdersByCategory(result);
        res.send(result);
    });
};

