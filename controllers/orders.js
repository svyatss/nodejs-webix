var express = require('express'),
  mongoose = require('mongoose'),
  Orders = mongoose.model('Orders'),
  ObjectId = require('mongodb').ObjectId;


exports.getRecords = function(req,res){
    Orders.countRecords(function(err,result){
        var total = result;
        Orders.getRecords(req, res, function(err,result){
            result = Orders.replaceId(result);
            res.send({data:result, pos:req.query.start, total_count:total});
        });
    });
}

exports.getStatuses = function(req,res){
    Orders.getStatuses(req, function(err,result){
        res.send(result);
    });
}

exports.addRecord = function(req,res){
    Orders.addRecord(req, function(err, result){
        res.send({newid:result._id});
    });
}

exports.updateRecord = function(req,res){
    console.log(req.body.id);
    var id = ObjectId(req.body.id);
    console.log(req.body);
    console.log(id);
    Orders.update({"_id":id}, req.body, function(err, result){
        res.send({});
    });
}

exports.deleteRecord = function(req, res){
    var id = ObjectId(req.body.id);
    Orders.remove({"_id":id}, function(err, result){
        res.send({});
    });
}