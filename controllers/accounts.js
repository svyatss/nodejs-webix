var express = require('express'),
  mongoose = require('mongoose'),
  Accounts = mongoose.model('Accounts'),
  ObjectId = require('mongodb').ObjectId;

exports.getRecords = function(req,res){
    Accounts.getRecords(req, res, function(err,result){
        res.send(result);
    });
}

exports.updateRecord = function(req,res){
    var id = ObjectId(req.body.id);
    Accounts.update({"_id":id}, req.body, function(err,result){
        res.send({});
    });
}

exports.addRecord = function(req,res){
    Accounts.addRecord(req, function(err,result){
        res.send({});
    });
}

exports.getRecordsSecurity = function(req,res){
    Accounts.getRecordsSecurity(req,res, function(err,result){
        res.send(result);
    });
}