var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var multer = require('multer');
var upload = multer({ dest: 'public/uploads' });

module.exports = function(controller){

    router.get('/', controller.getRecords);
    router.post('/', controller.addRecord);
    router.get('/getByCategory/:id', controller.getRecordsByCategory);
    router.post('/uploads', upload.single('upload'), controller.upload)
    router.get('/categories', controller.getCategories);
    router.put('/:id', controller.updateRecord);
    router.delete('/:id', controller.deleteRecord);

    return router;
}