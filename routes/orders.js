var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

module.exports = function(controller){

    router.get('/', controller.getRecords);
    router.post('/', controller.addRecord);
    router.get('/status', controller.getStatuses);
    router.put('/:id', controller.updateRecord);
    router.delete('/:id', controller.deleteRecord);

    return router;
}