var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

module.exports = function(controller){

    router.get('/accounts', controller.getRecords);
    router.put('/accounts/:id', controller.updateRecord);
    router.post('/accounts', controller.addRecord);
    router.get('/security', controller.getRecordsSecurity);

    return router;
}