var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');


router.get('/', function(req,res,next){
    products.distinct('category', function(err, result){
        res.send(result);
    });
});

router.get('/status', function(req,res,next){
    orders.distinct('status', function(err,result){
        res.send(result);
    });
});

router.get('/:id', function(req,res,next){
    var category = url.parse(req.url).pathname.replace("/", "");
    products.aggregate(
        [ {$match : { category : category }},
            { 
                $lookup:{
                    from: "orders",
                    localField: "code",
                    foreignField:"specification",
                    as: "joinData"
                }, 
            },
            {$project: {data: "$joinData"}}
        ],
        function(err, result){
            var item = [];
            for (val of result){
                if(val.data[0]){
                    val.data[0].id = val.data[0]._id;
                    delete val.data[0]._id;
                    item.push(val.data[0]);
                }
            }
            res.send(item);
        }
    );
});

router.put('/', function(req,res,next){
    req.body = JSON.parse(req.body.data_orders)
    var id = ObjectId(req.body.id);
    delete req.body.id;
    orders.updateOne(
        {"_id":id},
        {
            $set: req.body
        }, function(err,result){
            res.send({});
        }
    );
});

module.exports = router;