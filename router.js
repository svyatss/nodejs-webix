var productsController = require('./controllers/products.js');
var productsRouter = require('./routes/products');

var ordersController = require('./controllers/orders');
var ordersRouter = require('./routes/orders');

var accountsController = require('./controllers/accounts');
var settingsRouter = require('./routes/settings');

module.exports = function(app){

    app.get('/', function(req,res){
        res.render('index.jade');
    });
    app.use('/products', productsRouter(productsController));
    app.use('/orders', ordersRouter(ordersController));
    app.use('/settings', settingsRouter(accountsController));


}