define([],function(){

    var save = function(data){
        webix.ajax().post("/settings/accounts", data, function(text, xml, xhr){
            if(text==="{}"){
                webix.message("Row has been updated successfully");
            } else {
                webix.message("Row hasn't been updated");
            }
        });
    }

    var upload = "/products/uploads";

    return {
        save:save,
        upload:upload
    }

});