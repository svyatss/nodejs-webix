define([], function(){

    // var urlGet = "server/products.php";
    var getCategories = "products/categories";

    var collection = new webix.DataCollection({
        url:"/products",
        save:"rest->/products"
    });

    return {
        data: collection,
        getCategories: getCategories
    }

});