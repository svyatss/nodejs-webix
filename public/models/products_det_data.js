define([], function(){

    var urlGetOrders = "products/getByCategory";
    var getStatus = "/orders/status";
    var updateUrl = "/orders";

    var collection = new webix.DataCollection({
        url:"/products/categories",
    });

    return {
        data: collection,
        urlGetOrders: urlGetOrders,
        getStatus: getStatus,
        updateUrl: updateUrl
    }
});