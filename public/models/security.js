define([],function(){

    var collection = new webix.DataCollection({
        url:"rest->settings/security",
    });

    var change = function(data){
        webix.ajax().headers({"Content-type":"application/json"}).put("settings/accounts"+"/"+data.id, JSON.stringify(data), function(text, xml, xhr){
            if(text==="{}"){
                webix.message("Row has been updated successfully");
            } else {
                webix.message("Row hasn't been updated");
            }
        });
    }

    return{
        data:collection,
        change:change
    }

});