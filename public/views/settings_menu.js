define([],function(){

    var header = {
        type:"header", template:"Settings"
    };

    var menu = {
        view:"menu", id:"settings:menu", 
        width:180, layout:"y", select:true,
        template:"#value# ",
        data:[
            { value:"Account settings",             id:"account",      href:"#!/top/settings_menu/account"},
            { value:"Security settings",            id:"security",     href:"#!/top/settings_menu/security"},
            { value:"Email settings",               id:"email",        href:"#!/top/settings_menu/email"},
        ]
    };

    var ui = {
        type:"line", cols:[
            { type:"clean", css:"app-left-panel",
                padding:10, margin:20, borderless:true, rows: [ header, menu ]},
            { rows:[ { height:10}, 
                { type:"clean", css:"app-right-panel", padding:4, rows:[
                    { $subview:true } 
                ]}
            ]}
        ]
    };

    return {
        $ui: ui,
        $menu: "settings:menu"
    };
});
