define([
    "models/products_data"
    ], function(products_data){

    var edit_popup = {
                view:"popup",
                id:"popup_window",
                width:400,
                height:400,
                left: window.innerWidth/2-200,
                body: {view:"form", name:"popup_form", elements:[
                    {view:"text", name:"code", placeholder:"code"},
                    {view:"text", name:"name", placeholder:"name"},
                    {view:"select", options:products_data.getCategories, name:"category"},
                    {view:"text", name:"price", placeholder:"price"},
                    {
                        view:"uploader",
                        id: "image_uploader",
                        value:"Choose image to upload",
                        upload:"/products/uploads",
                        datatype:"json",
                        autosend: false
                    },
                    {view:"button", type:"iconButton", label:"upload", icon:"upload", id:"upload_button"},
                    {cols:[
                            {view:"button", id:"save", value:"save"},
                            {view:"button", id:"cancel", value:"cancel"}
                        ]}
                ]}
    };

    return {
        popup: edit_popup
    }

});