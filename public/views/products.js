define([
    "models/products_data",
    "views/popup"
    ], function(products_data, popup){

    var edit_popup = popup.popup;

    var products_tab = {rows:[
            {template:"Products", gravity:0.5},
            {cols:[
                    {view: "button", value:"refresh", id:"refresh", type:"iconButton", label:"Refresh", icon:"refresh", gravity:1.5, click:refresh},
                    {view: "button", value:"export", id:"export", type:"iconButton", label:"Export to Excel", icon:"table", gravity:1, width: 150, click:expxls},
                    {view:"button", value:"new", width:150, click:add_row},
                    {gravity:10}   
                ]},

            {view:"template", id:"image_prods", template:"Product image", height:100},

            {view: "datatable", select:"row", columns:[
                {id:"id", header:"№", width:50},
                {id:"code", editor:"text",header:["Code", {content:"numberFilter"}], width:100},
                {id:"name", editor:"text", header:["Name", {content:"textFilter"}], width:100, fillspace:true},
                {id:"category", editor:"select", options:products_data.getCategories, header:["Category", {content:"selectFilter"}], width:150},
                {id:"price", editor:"text", header:"Price", width:100, sort:"int"},
                {id:"quantity", header:"Quantity", width:100, sort:"int"},
                {id:"status", header:"Status", width:150, sort:"string"},
                {id:"delete", header:"Delete", width:100, template:"<input type='button' value='Delete' class='delete_button'>"},

            ], id:"products_table", editable:true, editaction:"none", gravity:12, onClick:{delete_button:delete_row}, on:{
                "onItemClick":dt_click,
                "onItemDblClick":dbl_click,
                //"onDataUpdate":on_data_update
            }}
        ],id:"products_tab", type:"clean"};

    function expxls(){
        webix.toExcel($$("products_table"));
    }


    function delete_row(id, ev){

        products_data.data.remove(ev.row);
        return false;

    }

    function add_row(){
        products_data.data.add({code:100, name:"Desk", category:"Home", price:100, quantity:100, status:"not published"});
    }

    function dt_click(id,e){
        var item = $$("products_table").getItem(id);

        var cy = e.clientY+20;
        var ph = $$("popup_window").$height;
        var wh = window.innerHeight;


        if (cy + ph > wh){
            cy -= ph;
        };


        $$("popup_window").setPosition(popup.$left,cy);
        
        $$("popup_window").getBody().setValues({
            code: item.code,
            name: item.name,
            category: item.category,
            price: item.price
        });

        if(item.image_src){
            $$("image_prods").setHTML("<image src='/uploads/"+item.image_src+"' style='max-width:100%; max-height:100%'>");
        } else {
            $$("image_prods").setHTML("No image");  
        }
        
        $$("popup_window").show();

    }

    function save_click(id){

        var item_id = $$("products_table").getSelectedId();
        var item = $$("products_table").getItem(item_id);
        var values = $$("popup_window").getBody().getValues();
        item.code = values.code;
        item.name = values.name;
        item.category = values.category;
        item.price = values.price;
        $$("products_table").updateItem(item_id, item);
        $$("popup_window").hide();

    }

    function dbl_click(id){
        $$("products_table").editCell(id.row, id.column);
    }


    function refresh(){
        $$("products_table").load(products_data.data.data.url);
    }


    function init(view, $scope){

            var popup = $scope.ui(edit_popup);

            $$("save").attachEvent("onItemClick", save_click);

            $$("image_uploader").attachEvent("onFileUpload", function(item, response){
                $$("image_prods").setHTML("<image src='/uploads/"+response.filename+"' style='max-width:100%; max-height:100%'>");
                refresh();
                webix.message("uploaded");
            });

            $$("cancel").attachEvent("onItemClick",function(){
                popup.hide();
            });

            $$("upload_button").attachEvent("onItemClick", function(){
                var item_id = $$("products_table").getSelectedId();
                var item = $$("products_table").getItem(item_id);
                var file = $$("image_uploader").files.getLastId();
                if(file){
                    $$("image_uploader").send(file, {id_prod: item.id});
                }
            });

            $$("products_table").sync(products_data.data);

    };

    return {
        $ui: products_tab,
        $oninit:init,
    };
});

