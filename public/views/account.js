define([
    "models/accounts"
    ],function(accounts){

    var ui = {view:"form", id:"account_settings", type:"space", elements:[
        {rows:[
                {gravity:1},
                {cols:[
                        {gravity:1},
                        {rows:[
                                {view:"text", name:"name", placeholder:"name"},
                                {view:"text", name:"surname", placeholder:"surname"},
                                {view:"text", name:"email", placeholder:"email"},
                                {view:"text", name:"phone", placeholder:"phone"}
                            ]},
                        {gravity:0.3},
                        {rows:[
                                {view:"text", name:"department", placeholder:"department"},
                                {view:"text", name:"position", placeholder:"position"},
                                {
                                    view:"uploader",
                                    id: "image_uploader_acc",
                                    value:"Choose image to upload",
                                    upload:accounts.upload,
                                    datatype:"json",
                                    autosend: false,
                                    on:{
                                        "onFileUpload":function(){
                                            webix.message("uploaded");
                                        }
                                    }
                                },
                                {view:"button", value:"upload", click:upload},
                                {view:"template", id:"image_template", height:100},
                            ]},
                        {gravity:1}
                    ]},
                {gravity:0.3},
                {cols:[
                        {gravity:1},
                        {view:"button", value:"Add", width:100, id:"add_button", click:add_account},
                        {gravity:0.3},
                        {view:"button", value:"Cancel", width:100},
                        {gravity:1}
                    ]},
                {gravity:3}
            ]}
    ]};

    var current_image;

    function upload(){
        var file = $$("image_uploader_acc").files.getLastId();
        if(file){
            $$("image_uploader_acc").send(function(response){
                $$("image_template").setHTML("<image src='/uploads/"+response.filename+"' style='max-width:100%; max-height:100%'>");
                current_image = response.filename;

            });
        }
    };

    function add_account(){
        var values = $$("account_settings").getValues();
        values.security = 0;
        if(current_image){
            values.image_src = current_image;
        }
            accounts.save(values);
        };

    return {
        $ui:ui,
        $oninit:function(view, $scope){

        }
    }

});