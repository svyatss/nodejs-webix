define([
    "models/products_det_data"
],function(products_det_data){

    var ui = {rows:[
            {template:"Product detalization", gravity:0.5},
            {cols:[
                    {rows:[
                            {
                                view:"list",
                                select:"true",
                                gravity:10,
                                id: "category_list"
                            },
                        ]},
                    {view:"datatable", columns:[
                        {id:"id", header:"№", width:50},
                        {id:"date_s", editor:"text", header:"Date of order", width:100, fillspace:true},
                        {id:"status", editor:"select", options:products_det_data.getStatus, header:"Status", width:100},                        
                    ], id:"orders_table_short", editable:true, editaction:"none", select:true, gravity:1},
                    {rows:[
                            {view:"template", id:"detalization", template:"Order: №#id#<br>Salesperson: #salesperson#<br>Customer: #customer#<br>Status of order: #status#<br>Total fee: #fee#<br>Shipping company: #shipping#<br>Date of order: #date_s#", data:[
                                {id:"",salesperson:"",customer:"",status:"",fee:"",shipping:"",date_s:""}
                            ]}
                        ], gravity:1}
                ],gravity:12},
        ], id:"product_detalization_tab", gravity:10};

    return {
        $ui: ui,
        $oninit:function(view, $scope){
            $$("category_list").sync(products_det_data.data);
            $$("orders_table_short").attachEvent("onDataUpdate", function(id, data){
                webix.ajax().headers({"Content-type":"application/json"}).put(products_det_data.updateUrl+"/"+data.id, JSON.stringify(data), function(text, xml, xhr){
                    if(text==="{}"){
                        webix.message("Row has been updated successfully");
                    } else {
                        webix.message("Row hasn't been updated");
                    }
                });
            });

            $$("orders_table_short").attachEvent("onItemDblClick", function(id){
                $$("orders_table_short").editCell(id.row, id.column);
            });

            // $$("category_list").load(products_det_data.urlGetCats);

            $scope.on($$("category_list"), "onAfterSelect", function(id){
                $$("orders_table_short").clearAll();
                $$("orders_table_short").load(products_det_data.urlGetOrders+"/"+id, function(){
                    console.log($$("orders_table_short").getItem($$("orders_table_short").getFirstId()));
                });
            })

            $scope.on($$("orders_table_short"), "onAfterSelect", function(id){
                $$("detalization").parse($$("orders_table_short").getItem(id));
            });
        }
    };
    
});
