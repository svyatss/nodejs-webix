define([], function(){

    var specs = {
                view:"popup",
                id:"popup_spec",
                width:400,
                height:400,
                left: window.innerWidth/2-200,
                body: {template:"Specification info", id:"specs_body"}
    };

    return {
        popup: specs
    }

});