define([
	"app"
],function(app){

	var header = {
		type:"header", template:app.config.name
	};

	var menu = {
		view:"menu", id:"top:menu", 
		width:180, layout:"y", select:true,
		template:"#value# ",
		data:[
            { value:"Products",             id:"products",      href:"#!/top/products"},
            { value:"Product Detalization", id:"product_det",   href:"#!/top/product_det"},
            { value:"Orders",               id:"orders",        href:"#!/top/orders"},
            { value:"Settings",             id:"settings_menu",      href:"#!/top/settings_menu/account"}
		]
	};

	var ui = {
		type:"line", cols:[
			{ type:"clean", css:"app-left-panel",
				padding:10, margin:20, borderless:true, rows: [ header, menu ]},
			{ rows:[ { height:10}, 
				{ type:"clean", css:"app-right-panel", padding:4, rows:[
					{ $subview:true } 
				]}
			]}
		]
	};

	return {
		$ui: ui,
		$menu: "top:menu"
	};
});
