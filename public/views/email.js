define([
    "models/email"
    ],function(email){

    var ui = {view:"form", id:"email_form", visibleBatch:"b1", elements:[
            {rows:[
                {cols:[
                    {gravity:1},
                    {rows:[
                        {view:"checkbox", id:"check1", name:"1", labelRight:"to show the sender's avatar", value:1, on:{
                            "onChange":checkbox_handler
                        }},
                        {view:"checkbox", id:"check2", labelRight:"to show the first line of the email", value:1, on:{
                            "onChange":checkbox_handler
                        }},
                        {view:"checkbox", id:"check3", labelRight:"to turn on sound norifications about letter", value:1, on:{
                            "onChange":checkbox_handler
                        }},
                        {view:"button", value:"lock recciving emails from", on:{"onItemClick":show}}
                        ]},
                    {gravity:1}
                    ]},
                    {view:"datatable", columns:[
                        {id:"department", header:"Department", width:150},
                        {id:"position", header:"Position", width:150},
                        {id:"salesperson", header:"Salesperson", width:150},
                        {id:"status", header:"Lock", template:"{common.checkbox()}"}
                    ], id:"lock_email_table"},
                    {view:"toolbar", id:"toolbar1", elements:[
                        {template:"control set 1"},
                        {view:"button", value:"button1"},
                        {view:"button", value:"button2"},
                        {view:"button", value:"button3"},
                    ]},{view:"toolbar", id:"toolbar2", elements:[
                        {template:"control set 2"},
                        {view:"button", value:"button1"},
                        {view:"button", value:"button2"},
                        {view:"button", value:"button3"},
                    ]},{view:"toolbar", id:"toolbar3", elements:[
                        {template:"control set 3"},
                        {view:"button", value:"button1"},
                        {view:"button", value:"button2"},
                        {view:"button", value:"button3"},
                    ]},
                    {gravity:1}
            ]}
    ]};

    function show(){
        if($$("lock_email_table").isVisible()){
            $$("lock_email_table").hide();
            return;
        }
        $$("lock_email_table").show();
    }

    function checkbox_handler(){

        if($$("check1").getValue()===1){
            $$("toolbar1").show();
        } else {
            $$("toolbar1").hide();
        }
        if($$("check2").getValue()===1){
            $$("toolbar2").show();
        } else {
            $$("toolbar2").hide();
        }
        if($$("check3").getValue()===1){
            $$("toolbar3").show();
        } else {
            $$("toolbar3").hide();
        }
    }

    return{
        $ui: ui,
        $oninit:function(){
            $$("lock_email_table").sync(email.data);
            $$("lock_email_table").hide();
        }
    }    

});