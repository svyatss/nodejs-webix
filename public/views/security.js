define([
    "models/security"
    ],function(security){

    var ui = {cols:[
            {gravity:0.5},
            {rows:[
                {gravity:1},
                {view:"richselect", id:"my_combo",
                label:"give access", options:[], on:{
                    "onChange":combo_handler
                }},
                {view:"checkbox", labelRight:"give access", id:"check", click:check_handler},
                {cols:[
                        {view:"button", value:"save", click:save_click},
                        {view:"button", value:"cancel"},                    
                    ]},
                {gravity:1}
            ]},
            {gravity:0.5}
        ]};

    function save_click(){
        var list = $$("my_combo").getPopup().getList();
        var item = list.getItem($$("my_combo").getValue());
        security.change(item);
    }

    function combo_handler(id){
        $$("check").show();
        var list = $$("my_combo").getPopup().getList();
        $$("check").setValue(list.getItem(id).security);
    }

    function check_handler(){
        var list = $$("my_combo").getPopup().getList();
        var item = list.getItem($$("my_combo").getValue());
        item.security = $$("check").getValue();
    }


    return{
        $ui: ui,
        $oninit:function(){
            var list = $$("my_combo").getPopup().getList();
            list.parse(security.data);
            $$("check").hide();
        }
    }    

});