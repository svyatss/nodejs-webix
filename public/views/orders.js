define([
    "models/orders_data",
    "views/popup_spec"
], function(orders_data, popup_spec){


    var ui = {rows:[
        {template:"Orders", gravity:0.5},
        {view: "datatable",
            columns:[
            {id:"id", header:"№", width:50},
            {id:"salesperson", editor:"text", header:["Salesperson",{content:"selectFilter"}], width:100},
            {id:"customer", editor:"text", header:["Customer",{content:"selectFilter"}], width:100, fillspace:true},
            {id:"status", editor:"select", options:orders_data.getStatus, header:"Status of order", width:150, sort:"server"},
            {id:"shipping", editor:"text", header:"Shipping", width:100, sort:"server"},
            {id:"fee", template:"$#fee#", header:"Fee", width:100, sort:"server"},
            {id:"date_s", header:"Date", sort:"server", format:webix.Date.dateToStr("%Y-%m-%d")},
            {id:"specification", editor:"text", header:"Specification", template:"<span class='webix_icon fa-file-o spec_button'></span>#specification#", width:100},
            {id:"delete", header:"Delete", width:100, template:"<input type='button' value='Delete' class='delete_button'>"},

        ], id:"orders_table", editable:true, editaction:"none", url:orders_data.urlGet, datafetch:15, datathrottle: 300, loadahead:5, pager:"orders_pager", gravity:12, onClick:{
            delete_button: delete_row,
            spec_button: show_spec
        }},
            {view:"pager", size:10, id:"orders_pager"}
    ],id:"orders_tab", type:"clean", gravity: 10};

    function delete_row(id, data){
        data = $$("orders_table").getItem(data);
        console.log(data);
        webix.confirm("Are you sure?", function(result){
            if(result){
                webix.ajax().headers({"Content-type":"application/json"}).del(orders_data.urlGet+"/"+data.id, JSON.stringify(data), function(text, xml, xhr){
                    if(text==="{}"){
                        webix.message("row deleted successfully");
                        $$("orders_table").remove(data.id);
                        $$("orders_table").render();
                    }
                });
            } 
        });
    }

    function show_spec(ev, id){
        $$("popup_spec").show();
    }


    return {
        $ui: ui,
        $oninit: function(view, $scope){

            var popup = $scope.ui(popup_spec.popup);

            $$("orders_table").attachEvent("onDataUpdate", function(id, data){
                webix.ajax().headers({"Content-type":"application/json"}).put(orders_data.urlGet+"/"+data.id, JSON.stringify(data), function(text, xml, xhr){
                    if(text==="{}"){
                        webix.message("Row has been updated successfully");
                    } else {
                        webix.message("Row hasn't been updated");
                    }
                });
            });

            $$("orders_table").attachEvent("onItemDblClick", function(id){
                $$("orders_table").editCell(id.row, id.column);
            });

        }
    };
    
});