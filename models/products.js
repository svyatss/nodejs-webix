var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  url = require('url');

var ProductsSchema = new Schema({
  code: Number,
  name: String,
  category: String,
  price: Number,
  quantity: Number,
  status: String,
  image_src: String
});

ProductsSchema.statics.getRecords = function(req, cb){
    return this.find().lean().exec(cb);
}

ProductsSchema.statics.getCategories = function(req, cb){
    return this.find().distinct('category', cb);
}

ProductsSchema.statics.replaceId = function(ids){
    for (val of ids){
        val.id = val._id;
        delete val._id;
    }
    return ids;
}

ProductsSchema.statics.addRecord = function(req, cb){
    var doc = this.createInstance(req.body);
    return doc.save(cb);
}

ProductsSchema.statics.getOrdersByCategory = function(req,cb){
    var category = url.parse(req.url).pathname.replace("/getByCategory/", "");
    return this.aggregate(
        [ {$match : { category : category }},
            {
                $lookup:{
                    from: "orders",
                    localField: "code",
                    foreignField:"specification",
                    as: "join"
                }
            },
            {$project: {data:"$join"}}
        ], cb);
}

ProductsSchema.statics.helperGetOrdersByCategory = function(result){
    var data = [];
    for (val of result){
        if(val.data.length!=0){
            val.data[0].id = val.data[0]._id;
            data.push(val.data[0]);
        }
    }
    return data;
}

ProductsSchema.statics.createInstance = function(body){
    var doc = new this(body);
    return doc;
}

mongoose.model('Products', ProductsSchema);