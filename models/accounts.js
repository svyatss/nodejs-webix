var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var AccountsSchema = new Schema({
  name: String,
  surname: String,
  email: String,
  phone: String,
  position: String,
  department: String,
  status: { type: Number, default: 0 },
  security: { type: Number, default: 0 }
});

AccountsSchema.statics.getRecords = function(req, res, cb){    
    return this.aggregate(
        [
            {$project: {id:"$_id", salesperson:{$concat:["$name"," ","$surname"]}, status:1, position:1, department:1}}
        ], cb);
}

AccountsSchema.statics.getRecordsSecurity = function(req,res,cb){
    return this.aggregate(
        [
            {$project: {id:"$_id", value:{$concat:["$name", " ", "$surname"]}, security:1}}
        ],cb);
}

AccountsSchema.statics.getStatuses = function(req, cb){
    return this.find().distinct('status', cb);
}

AccountsSchema.statics.replaceId = function(ids){
    for (val of ids){
        val.id = val._id;
        delete val._id;
    }
    return ids;
}

AccountsSchema.statics.addRecord = function(req, cb){
    var doc = this.createInstance(req.body);
    return doc.save(cb);
}

AccountsSchema.statics.createInstance = function(body){
    var doc = new this(body);
    return doc;
}

mongoose.model('Accounts', AccountsSchema);