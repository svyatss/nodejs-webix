var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var OrdersSchema = new Schema({
  salesperson: String,
  customer: String,
  status: String,
  shipping: String,
  fee: Number,
  date_s: String,
  specification: Number
});

OrdersSchema.statics.countRecords = function(cb){
    return this.count(cb);
}

OrdersSchema.statics.getRecords = function(req, res, cb){

    if (req.query.continue){
        var start = parseInt(req.query.start);
        var count = parseInt(req.query.count);
    } else {
        var start = 0;
        var count = 10;
    }

    if(req.query.sort){

        var fieldToSort = Object.keys(req.query.sort)[0];
        var sortType = req.query.sort[fieldToSort];
        if (sortType === "asc") sortType = 1;
        else sortType = -1;
        var sort = {};
        sort[fieldToSort] = sortType;

    }
    
    return this.find().skip(start).limit(count).sort(sort).lean().exec(cb);
}

OrdersSchema.statics.getStatuses = function(req, cb){
    return this.find().distinct('status', cb);
}

OrdersSchema.statics.replaceId = function(ids){
    for (val of ids){
        val.id = val._id;
        delete val._id;
    }
    return ids;
}

OrdersSchema.statics.addRecord = function(req, cb){
    var doc = this.createInstance(req.body);
    return doc.save(cb);
}

OrdersSchema.statics.createInstance = function(body){
    var doc = new this(body);
    return doc;
}

mongoose.model('Orders', OrdersSchema);